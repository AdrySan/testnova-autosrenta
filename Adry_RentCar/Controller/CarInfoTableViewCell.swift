//
//  CarInfoTableViewCell.swift
//  Adry_RentCar
//
//  Created by MacPro on 11/26/19.
//  Copyright © 2019 MacPro. All rights reserved.
//

import UIKit

class CarInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var lblMarca: UILabel!
    @IBOutlet weak var lblModelo: UILabel!
    @IBOutlet weak var lblPrecioRenta: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
