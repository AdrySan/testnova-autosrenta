//
//  DetailCarViewController.swift
//  Adry_RentCar
//
//  Created by MacPro on 11/26/19.
//  Copyright © 2019 MacPro. All rights reserved.
//

import UIKit

class DetailCarViewController: UIViewController {
    
    let confirmRentCar = ConfirmRentCarViewController()
    var car: Car?
    var text:String = ""
    
    let pickerDateInicio = UIDatePicker()
    
    @IBOutlet weak var imgCar: UIImageView!
    @IBOutlet weak var viewInfoCar: UIView!
    @IBOutlet weak var viewRentCar: UIView!
    @IBOutlet weak var btnRentar: UIButton!
    
    //oulets View Info Car
    @IBOutlet weak var lblMarca: UILabel!
    @IBOutlet weak var lblModelo: UILabel!
    @IBOutlet weak var lblPrecioRenta: UILabel!
    @IBOutlet weak var lblPasajeros: UILabel!
    @IBOutlet weak var lblTransmision: UILabel!
    @IBOutlet weak var lblAireAcondicionado: UILabel!
    @IBOutlet weak var lblMaletas: UILabel!
    @IBOutlet weak var lblPuertas: UILabel!
    
    //Oulets View Rent Car
    @IBOutlet weak var txtFechaInicio: UITextField!
    @IBOutlet weak var txtFechaFin: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configOulets()
        configViewStyle()
        configPickersDate()
    }
    override func viewDidDisappear(_ animated: Bool) {
        txtFechaFin.text = ""
        txtFechaInicio.text = ""
    }
    
    // MARK: - Configuraciones
    
    func configOulets (){
        lblMarca.text = car?.marca
        lblModelo.text = car?.modelo
        lblPrecioRenta.text = car?.precioRenta
        lblPasajeros.text = car?.pasajeros
        lblTransmision.text = car?.transmision
        lblAireAcondicionado.text = car?.aireAcondicionado ?? false ? "Si":"No"
        lblMaletas.text = car?.maletas
        lblPuertas.text = car?.marca
        txtFechaInicio.text = ""
        txtFechaFin.text = ""
    }
    
    func configViewStyle(){
        //Corners
        imgCar.layer.cornerRadius = 10
        viewInfoCar.layer.cornerRadius = 7
        viewRentCar.layer.cornerRadius = 7
    }
    
    func configPickersDate(){
        self.txtFechaInicio.setInputViewDatePicker(target: self, selector: #selector(tapeDoneDatePicker))
        self.txtFechaFin.setInputViewDatePicker(target: self, selector: #selector(tapeDoneDatePickerFin))
    }
    
    // MARK: - Eventos
    @objc func tapeDoneDatePicker() {
        if let datePicker = self.txtFechaInicio.inputView as? UIDatePicker {
            let dateformatter = DateFormatter()
            dateformatter.dateStyle = .medium
            self.txtFechaInicio.text = dateformatter.string(from: datePicker.date)
        }
        txtFechaInicio.resignFirstResponder()
    }
    
    @objc func tapeDoneDatePickerFin() {
        if let datePicker = self.txtFechaFin.inputView as? UIDatePicker {
            let dateformatter = DateFormatter()
            dateformatter.dateStyle = .medium
            self.txtFechaFin.text = dateformatter.string(from: datePicker.date)
        }
        txtFechaFin.resignFirstResponder()
    }
    
    // MARK: - Actions
    @IBAction func btnRentar(_ sender: UIButton) {
        let rent = RentModel(car: car, fechaInicio: self.txtFechaInicio.text, fechaFin: self.txtFechaFin.text)
        self.confirmRentCar.rentModel = rent
        self.navigationController?.pushViewController(confirmRentCar, animated: true)
    }
}

// MARK: - Extension UITextField - config DatePicker
extension UITextField{
    func setInputViewDatePicker(target: Any, selector: Selector) {
        let screenWidth = UIScreen.main.bounds.width
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))
        datePicker.datePickerMode = .date
        self.inputView = datePicker

        let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0))
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancel = UIBarButtonItem(title: "Cancelar", style: .plain, target: nil, action: #selector(tapCancelButton))
        let barButton = UIBarButtonItem(title: "Aceptar", style: .plain, target: target, action: selector)
        toolBar.setItems([cancel, flexible, barButton], animated: false)
        self.inputAccessoryView = toolBar
        }
       
       @objc func tapCancelButton() {
           self.resignFirstResponder()
       }
}
