//
//  CarModel.swift
//  Adry_RentCar
//
//  Created by MacPro on 11/25/19.
//  Copyright © 2019 MacPro. All rights reserved.
//

import Foundation

struct Car: Codable {
    let marca : String?
    let modelo : String?
    let precioRenta : String?
    let pasajeros : String?
    let transmision : String?
    let maletas : String?
    let aireAcondicionado : Bool?
    let puertas : String?
}
